-- Use the newly created database
USE ETLDatabase;
GO

-- Create a table to store the data with nullable columns
CREATE TABLE ETLTable
(
    TpepPickupDatetime DATETIME NULL,
    TpepDropoffDatetime DATETIME NULL,
    PassengerCount INT NULL,
    TripDistance DECIMAL(18, 2) NULL,
    StoreAndFwdFlag NVARCHAR(3) NULL,
    PULocationID INT NULL,
    DOLocationID INT NULL,
    FareAmount DECIMAL(18, 2) NULL,
    TipAmount DECIMAL(18, 2) NULL
);
