﻿namespace ConsoleApp;

public class CsvData
{
	public DateTime? TpepPickupDatetime { get; set; }
	public DateTime? TpepDropoffDatetime { get; set; }
	public int? PassengerCount { get; set; }
	public decimal? TripDistance { get; set; }
	public string? StoreAndFwdFlag { get; set; }
	public int? PuLocationId { get; set; }
	public int? DoLocationId { get; set; }
	public decimal? FareAmount { get; set; }
	public decimal? TipAmount { get; set; }
}
