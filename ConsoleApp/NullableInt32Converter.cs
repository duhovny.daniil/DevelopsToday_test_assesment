﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace ConsoleApp;

/// <summary>
/// Custom nullable parser to avoid CsvHelper exceptions.
/// </summary>
public class NullableInt32Converter : DefaultTypeConverter
{
	public override object? ConvertFromString(string? text, IReaderRow row, MemberMapData memberMapData)
	{
		if (string.IsNullOrEmpty(text))
		{
			return null; // Return null for empty values
		}

		if (int.TryParse(text, out var result))
		{
			return result;
		}

		return null; // Return null for invalid values
	}

	public override string? ConvertToString(object? value, IWriterRow row, MemberMapData memberMapData) => 
		value is null ? string.Empty : value.ToString();
}
