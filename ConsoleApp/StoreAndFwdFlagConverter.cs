﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace ConsoleApp;

public class StoreAndFwdFlagConverter : DefaultTypeConverter
{
	public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
	{
		if (string.Equals(text, "N", StringComparison.OrdinalIgnoreCase))
			return "No";

		if (string.Equals(text, "Y", StringComparison.OrdinalIgnoreCase))
			return "Yes";

		// Return the original value if it's not 'N' or 'Y'
		return text; 
	}

	public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
	{
		if (string.Equals(value as string, "No", StringComparison.OrdinalIgnoreCase))
			return "N";
		
		if (string.Equals(value as string, "Yes", StringComparison.OrdinalIgnoreCase))
			return "Y";

		return value?.ToString();
	}
}