﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;

namespace ConsoleApp;
public sealed class CsvDataMap : ClassMap<CsvData>
{
	public CsvDataMap()
	{
		Map(m => m.TpepPickupDatetime).Name("tpep_pickup_datetime").TypeConverter<DateTimeTypeConverter>();
		Map(m => m.TpepDropoffDatetime).Name("tpep_dropoff_datetime").TypeConverter<DateTimeTypeConverter>();
		Map(m => m.PassengerCount).Name("passenger_count").TypeConverter<NullableInt32Converter>();
		Map(m => m.TripDistance).Name("trip_distance");
		Map(m => m.StoreAndFwdFlag).Name("store_and_fwd_flag").TypeConverter<StoreAndFwdFlagConverter>();
		Map(m => m.PuLocationId).Name("PULocationID");
		Map(m => m.DoLocationId).Name("DOLocationID");
		Map(m => m.FareAmount).Name("fare_amount");
		Map(m => m.TipAmount).Name("tip_amount");
	}
}
