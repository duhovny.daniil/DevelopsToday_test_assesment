﻿using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;

namespace ConsoleApp;

internal class Program
{
	static void Main()
	{
		Console.WriteLine("Enter the path to your CSV file:");
		var csvFilePath = Console.ReadLine();

		while (!File.Exists(csvFilePath))
		{
			Console.Clear();
			Console.WriteLine("File not found. Please provide a valid file path.");
			csvFilePath = Console.ReadLine();
		}

		var csvConfig = new CsvConfiguration(CultureInfo.InvariantCulture)
		{
			Delimiter = ",",
			HasHeaderRecord = true
		};

		using var reader = new StreamReader(csvFilePath);
		using var csv = new CsvReader(reader, csvConfig);
		csv.Context.RegisterClassMap<CsvDataMap>();
		var records = csv.GetRecords<CsvData?>().ToList();

		var dataTable = new DataTable();
		dataTable.Columns.Add("TpepPickupDatetime", typeof(DateTime));
		dataTable.Columns.Add("TpepDropoffDatetime", typeof(DateTime));
		dataTable.Columns.Add("PassengerCount", typeof(int));
		dataTable.Columns.Add("TripDistance", typeof(decimal));
		dataTable.Columns.Add("StoreAndFwdFlag", typeof(string));
		dataTable.Columns.Add("PULocationID", typeof(int));
		dataTable.Columns.Add("DOLocationID", typeof(int));
		dataTable.Columns.Add("FareAmount", typeof(decimal));
		dataTable.Columns.Add("TipAmount", typeof(decimal));

		var uniqueRecords = new List<CsvData>();
		var duplicateRecords = new List<CsvData>();

		var duplicateIdentifierSet = new HashSet<string>();

		foreach (var record in records)
		{
			// Create a unique identifier based on the combination of pickup_datetime, dropoff_datetime, and passenger_count
			var identifier = $"{record.TpepPickupDatetime:yyyyMMddHHmmss}-{record.TpepDropoffDatetime:yyyyMMddHHmmss}-{record.PassengerCount}";

			if (duplicateIdentifierSet.Contains(identifier))
			{
				// This is a duplicate record
				duplicateRecords.Add(record);
			}
			else
			{
				// This is a unique record
				uniqueRecords.Add(record);
				duplicateIdentifierSet.Add(identifier);
			}
		}

		// Write the list of duplicates to a CSV file
		using var writer = new StreamWriter("duplicates.csv");
		using var csvWriter = new CsvWriter(writer, csvConfig);
		csvWriter.WriteRecords(duplicateRecords);

		// Define the time zones
		var estTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

		foreach (var record in uniqueRecords)
		{
			var pickupUtc = TimeZoneInfo.ConvertTimeToUtc(record.TpepPickupDatetime ?? DateTime.MinValue, estTimeZone);
			var dropoffUtc = TimeZoneInfo.ConvertTimeToUtc(record.TpepDropoffDatetime ?? DateTime.MinValue, estTimeZone);

			dataTable.Rows.Add(
				pickupUtc,
				dropoffUtc,
				record.PassengerCount,
				record.TripDistance,
				record.StoreAndFwdFlag,
				record.PuLocationId,
				record.DoLocationId,
				record.FareAmount,
				record.TipAmount
			);
		}

		Console.WriteLine("Enter connection string:");
		var connectionString = Console.ReadLine();
		using var sqlConnection = new SqlConnection(connectionString);
		sqlConnection.Open();

		using var bulkCopy = new SqlBulkCopy(sqlConnection);
		bulkCopy.DestinationTableName = "ETLTable";
		bulkCopy.BatchSize = 1000; // Set an appropriate batch size
		bulkCopy.BulkCopyTimeout = 600; // Set an appropriate timeout

		bulkCopy.WriteToServer(dataTable);
		Console.WriteLine($"Number of unique records inserted into the database: {uniqueRecords.Count}");
	}
}