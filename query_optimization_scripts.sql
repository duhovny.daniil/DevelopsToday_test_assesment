-- Find the PULocationId with the highest average tip_amount. 
-- For this query, we can create an index on the PULocationId and include the tip_amount column. 
-- This will allow the database to quickly aggregate tip amounts by pick-up location and find the maximum average tip amount.
CREATE INDEX IDX_Pickup_TipAmount ON ETLTable (PULocationId) INCLUDE (tip_amount);

-- Find the top 100 longest fares by trip_distance.
-- To efficiently retrieve the top 100 longest fares, we create an index on the trip_distance column in descending order. 
-- This allows to quickly fetch the longest fares without sorting the entire table.
CREATE INDEX IDX_Longest_TripDistance ON ETLTable (trip_distance DESC);

-- Find the top 100 longest fares by time spent traveling.
-- Similar to the previous query, we create an index on the relevant column (time_spent_traveling or a similar column) in descending order.
CREATE INDEX IDX_Longest_TimeSpent ON ETLTable (time_spent_traveling DESC);

-- Search with conditions involving PULocationId.
-- If our searches involve conditions on the PULocationId, ensure that we have an index on this column to speed up filtering based on pick-up location.
CREATE INDEX IDX_Pickup_Location ON ETLTable (PULocationId);
